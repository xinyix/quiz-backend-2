alter table goods add picturelink varchar(255);

update goods set picturelink='https://www.asiamarket.ie/pub/media/catalog/product/cache/image/700x700/e9c3970ab036de70892d86c6d221abfe/6/9/6900873000777_800x800.jpg'
where id = 1;

update goods set picturelink='https://i5.walmartimages.ca/images/Large/094/514/6000200094514.jpg'
where id = 2;

update goods set picturelink='https://images-na.ssl-images-amazon.com/images/I/81mEIp4PMBL._SL1500_.jpg'
where id = 3;
