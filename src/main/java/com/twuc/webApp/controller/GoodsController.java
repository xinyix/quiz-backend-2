package com.twuc.webApp.controller;

import com.twuc.webApp.contracts.CreateGoodsRequest;
import com.twuc.webApp.contracts.GetGoodsResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.exceptions.NameAlreadyExistException;
import com.twuc.webApp.repository.GoodsRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class GoodsController {
    private GoodsRepository goodsRepository;

    public GoodsController(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    @GetMapping("/goods")
    public ResponseEntity getGoods() {
        List<Goods> allGoods = goodsRepository.findAll();
        List<GetGoodsResponse> getGoodsResponses = allGoods.stream().map(GetGoodsResponse::new).collect(Collectors.toList());

        return ResponseEntity.status(200).contentType(MediaType.APPLICATION_JSON_UTF8).body(getGoodsResponses);
    }

    @PostMapping("/goods")
    public ResponseEntity createGoods(@RequestBody @Valid CreateGoodsRequest createGoodsRequest) {
        String name = createGoodsRequest.getName();
        if (goodsRepository.findByName(name).isPresent()) {
            throw new NameAlreadyExistException("商品名称已存在，请输入新的商品名称");
        } else {
            Goods goods = new Goods(createGoodsRequest.getName(), createGoodsRequest.getPrice(), createGoodsRequest.getUnit(), createGoodsRequest.getPicturelink());
            goodsRepository.saveAndFlush(goods);
            return ResponseEntity.status(201).build();
        }
    }

    @ExceptionHandler(NameAlreadyExistException.class)
    public ResponseEntity nameAlreadyExistExceptionHandler(NameAlreadyExistException exception) {
        return ResponseEntity.status(400).body(exception.getMessage());
    }
}

