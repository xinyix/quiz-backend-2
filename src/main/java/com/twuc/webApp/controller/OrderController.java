package com.twuc.webApp.controller;

import com.twuc.webApp.contracts.AddOrderRequest;
import com.twuc.webApp.contracts.GetOrderResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.OrderFromClient;
import com.twuc.webApp.repository.GoodsRepository;
import com.twuc.webApp.repository.OrderRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class OrderController {
    private OrderRepository orderRepository;

    private GoodsRepository goodsRepository;

    public OrderController(OrderRepository orderRepository, GoodsRepository goodsRepository) {
        this.orderRepository = orderRepository;
        this.goodsRepository = goodsRepository;
    }

    @GetMapping("/orders")
    public ResponseEntity getOrders() {
        List<OrderFromClient> orderFromClients = orderRepository.findAll();
        List<GetOrderResponse> getOrderResponses = orderFromClients.stream().map(GetOrderResponse::new).collect(Collectors.toList());

        return ResponseEntity.status(200).contentType(MediaType.APPLICATION_JSON_UTF8).body(getOrderResponses);
    }

    @PostMapping("/orders")
    public ResponseEntity addOrder(@RequestBody @Valid AddOrderRequest addOrderRequest) {
        Long goodsId = addOrderRequest.getGoodsId();
        Goods goods = goodsRepository.findById(goodsId).get();
        Optional<OrderFromClient> orderFound = orderRepository.findByGoods(goods);
        if (orderFound.isPresent()) {
            OrderFromClient orderFromClient = orderFound.get();
            Integer quantity = orderFromClient.getQuantity();
            orderFromClient.setQuantity(quantity + 1);
            orderRepository.flush();
        } else {
            OrderFromClient orderFromClient = new OrderFromClient(addOrderRequest.getQuantity());
            orderFromClient.setGoods(goods);
            orderRepository.saveAndFlush(orderFromClient);
        }
        return ResponseEntity.status(201).build();
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity deleteOrder(@PathVariable Long id) {
        Optional<OrderFromClient> orderFound = orderRepository.findById(id);
        orderFound.ifPresent(orderRepository::delete);
        orderRepository.flush();
        return ResponseEntity.status(200).build();
    }
}
