package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class OrderFromClient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Goods goods;

    @Column
    private Integer quantity;

    public OrderFromClient() {
    }

    public OrderFromClient(Integer quantity) {
        this.quantity = quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }
}
