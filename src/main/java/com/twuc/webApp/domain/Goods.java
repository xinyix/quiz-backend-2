package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
@Table(name = "goods")
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Float price;

    @Column
    private String unit;

    @Column
    private String picturelink;

//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "goods")
//    private OrderFromClient orderFromClient;

    public Goods() {
    }

    public Goods(String name, Float price, String unit, String picturelink) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.picturelink = picturelink;
    }

    public Long getId() {
        return id;
    }

    public String getPicturelink() {
        return picturelink;
    }

    public String getName() {
        return name;
    }

    public Float getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

//    public OrderFromClient getOrderFromClient() {
//        return orderFromClient;
//    }
//
//    public void setOrderFromClient(OrderFromClient orderFromClient) {
//        this.orderFromClient = orderFromClient;
//    }
}

