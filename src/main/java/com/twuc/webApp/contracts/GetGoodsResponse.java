package com.twuc.webApp.contracts;

import com.twuc.webApp.domain.Goods;

import java.util.Objects;

public class GetGoodsResponse {

    private Long id;
    private String name;
    private Float price;
    private String unit;
    private String picturelink;

    public GetGoodsResponse() {
    }

    public GetGoodsResponse(Goods goods) {
        Objects.requireNonNull(goods);

        this.id = goods.getId();
        this.name = goods.getName();
        this.price = goods.getPrice();
        this.unit = goods.getUnit();
        this.picturelink = goods.getPicturelink();
    }

    public String getPicturelink() {
        return picturelink;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Float getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}