package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;

public class AddOrderRequest {
    @NotNull
    private Long goodsId;
    @NotNull
    private Integer quantity;

    public AddOrderRequest(@NotNull Long goodsId, @NotNull Integer quantity) {
        this.goodsId = goodsId;
        this.quantity = quantity;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
