package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;

public class CreateGoodsRequest {
    @NotNull
    private String name;
    @NotNull
    private Float price;
    @NotNull
    private String unit;
    @NotNull
    private String picturelink;

    public CreateGoodsRequest(@NotNull String name, @NotNull Float price, @NotNull String unit, @NotNull String picturelink) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.picturelink = picturelink;
    }

    public String getName() {
        return name;
    }

    public Float getPrice() {
        return price;
    }

    public String getPicturelink() {
        return picturelink;
    }

    public String getUnit() {
        return unit;
    }
}
