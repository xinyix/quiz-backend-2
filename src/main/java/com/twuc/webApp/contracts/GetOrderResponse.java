package com.twuc.webApp.contracts;

import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.OrderFromClient;

import java.util.Objects;

public class GetOrderResponse {
    private Long id;
    private String name;
    private Float price;
    private String unit;
    private Integer quantity;

    public GetOrderResponse(OrderFromClient orderFromClient) {
        Objects.requireNonNull(orderFromClient);

        Goods goods = orderFromClient.getGoods();

        this.id = orderFromClient.getId();
        this.name = goods.getName();
        this.price = goods.getPrice();
        this.unit = goods.getUnit();
        this.quantity = orderFromClient.getQuantity();
    }

    public GetOrderResponse() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Float getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}
