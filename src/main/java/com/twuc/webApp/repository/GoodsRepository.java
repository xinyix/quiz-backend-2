package com.twuc.webApp.repository;

import com.twuc.webApp.domain.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
    Optional<Goods> findByName(String name);
}
