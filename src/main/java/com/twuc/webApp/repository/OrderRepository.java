package com.twuc.webApp.repository;

import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.OrderFromClient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<OrderFromClient, Long> {

    Optional<OrderFromClient> findByGoods(Goods goods);
}
