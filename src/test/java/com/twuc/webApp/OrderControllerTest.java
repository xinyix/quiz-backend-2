package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.AddOrderRequest;
import com.twuc.webApp.contracts.GetOrderResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.OrderFromClient;
import com.twuc.webApp.repository.GoodsRepository;
import com.twuc.webApp.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class OrderControllerTest extends ApiTestBase{
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private GoodsRepository goodsRepository;

    @Test
    void should_get_orders() throws Exception {
        saveOrder();

        String contentAsString = getMockMvc().perform(get("/api/orders"))
            .andExpect(status().is(200))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andReturn().getResponse().getContentAsString();

        GetOrderResponse[] getOrderResponses = new ObjectMapper().readValue(contentAsString, GetOrderResponse[].class);

        assertEquals(1, getOrderResponses.length);
        assertEquals("可乐", getOrderResponses[0].getName());
        assertEquals("瓶", getOrderResponses[0].getUnit());
    }

    @Test
    void should_add_order() throws Exception {
        Goods goods = new Goods("可乐", 2.5f, "瓶", "url");
        goodsRepository.saveAndFlush(goods);

        AddOrderRequest addOrderRequest = new AddOrderRequest(1L, 1);

        getMockMvc().perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(new ObjectMapper().writeValueAsString(addOrderRequest)))
            .andExpect(status().is(201));

        assertNotNull(orderRepository.findById(1L));
    }

    @Test
    void should_add_one_on_quantity_for_the_same_goods() throws Exception {
        saveOrder();

        AddOrderRequest addOrderRequest = new AddOrderRequest(1L, 1);

        getMockMvc().perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(new ObjectMapper().writeValueAsString(addOrderRequest)))
            .andExpect(status().is(201));

        assertEquals(Optional.of(2), Optional.of(orderRepository.findById(1L).get().getQuantity()));
    }

    @Test
    void should_delete_order() throws Exception {
        saveOrder();

        getMockMvc().perform(delete("/api/orders/1"))
            .andExpect(status().is(200));

        assertFalse(orderRepository.findById(1L).isPresent());
    }

    private void saveOrder() {
        Goods goods = new Goods("可乐", 2.5f, "瓶", "url");
        goodsRepository.saveAndFlush(goods);

        OrderFromClient orderFromClient = new OrderFromClient(1);
        orderFromClient.setGoods(goods);
        orderRepository.saveAndFlush(orderFromClient);
    }
}
