package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.CreateGoodsRequest;
import com.twuc.webApp.contracts.GetGoodsResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.repository.GoodsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
public class GoodsControllerTest extends ApiTestBase {

    @Autowired
    private GoodsRepository goodsRepository;

    @Test
    void should_get_all_goods() throws Exception {
        Goods goods = new Goods("可乐", 2.5f, "瓶", "url");
        goodsRepository.saveAndFlush(goods);

        String contentAsString = getMockMvc().perform(get("/api/goods"))
            .andExpect(status().is(200))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andReturn().getResponse().getContentAsString();

        GetGoodsResponse[] getGoodsResponses = new ObjectMapper().readValue(contentAsString, GetGoodsResponse[].class);

        Float price = 2.5f;
        assertEquals(1, getGoodsResponses.length);
        assertEquals("可乐", getGoodsResponses[0].getName());
        assertEquals("瓶",getGoodsResponses[0].getUnit());
        assertEquals("url",getGoodsResponses[0].getPicturelink());
        assertEquals(price, getGoodsResponses[0].getPrice());
    }

    @Test
    void should_create_goods() throws Exception {
        CreateGoodsRequest createGoodsRequest = new CreateGoodsRequest("电池", 1f, "个", "url");

        getMockMvc().perform(post("/api/goods")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(new ObjectMapper().writeValueAsString(createGoodsRequest)))
            .andExpect(status().is(201));

        assertNotNull(goodsRepository.findById(1L));
    }

    @Test
    void should_return_error_message_when_add_goods_that_already_exists() throws Exception {
        Goods goods = new Goods("可乐", 2.5f, "瓶", "url");
        goodsRepository.saveAndFlush(goods);

        CreateGoodsRequest createGoodsRequest = new CreateGoodsRequest("可乐", 2.5f, "瓶", "url");

        getMockMvc().perform(post("/api/goods")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(new ObjectMapper().writeValueAsString(createGoodsRequest)))
            .andExpect(status().is(400))
            .andExpect(content().string("商品名称已存在，请输入新的商品名称"));
    }
}
